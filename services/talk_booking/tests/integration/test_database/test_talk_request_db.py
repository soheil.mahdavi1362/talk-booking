import datetime
import uuid

from database import talk_request_db
from database.database import db
from models import Address, TalkRequest


def test_talk_request(client):
    """
    GIVEN talk request and database
    WHEN talk request is saved
    THEN in can accessed by its id or listed
    """
    talk_request = TalkRequest(
        id=str(uuid.uuid4()),
        event_time=datetime.datetime.utcnow(),
        address=Address(
            street="Sunny street 42",
            city="Awesome city",
            state="Best state",
            country="Ireland",
        ),
        duration_in_minutes=45,
        topic="Python type checking",
        requester="john@doe.com",
        status="PENDING",
    )

    talk_request_db.save(db, talk_request)

    assert talk_request_db.list_all()[0] == talk_request
    assert talk_request_db.get_by_id(talk_request.id) == talk_request
