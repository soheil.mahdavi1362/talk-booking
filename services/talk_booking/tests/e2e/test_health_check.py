import requests


def test_health_check():
    response = requests.get(
        "https://develop-1234.talk-book.<YOUR DOMAIN NAME>.com/health-check/"
    )

    assert response.status_code == 200
    assert response.text == "OK"
