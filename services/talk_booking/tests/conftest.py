import flask_migrate
import psycopg2
import pytest

from database.database import db
from web_app.app import app
from web_app.config import load_config


@pytest.fixture(scope="session")
def database():
    dsn_parts = load_config().SQLALCHEMY_DATABASE_URI.split("/")
    database_name = dsn_parts[-1]
    dsn = "/".join(
        dsn_parts[:-1] + ["postgres"]
    )  # to login to postgres database instead of application one
    con = psycopg2.connect(dsn)
    con.autocommit = True
    cur = con.cursor()
    cur.execute(f"DROP DATABASE IF EXISTS {database_name};")
    cur.execute(f"CREATE DATABASE {database_name};")


@pytest.fixture
def client(database):
    app.config["TESTING"] = True
    with app.app_context():
        with app.test_client() as client:
            flask_migrate.upgrade()
            db.drop_all()
            db.create_all()
            yield client
            db.session.close()
            db.drop_all()
            db.session.remove()
            db.engine.dispose()
