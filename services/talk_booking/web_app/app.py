import pathlib
import uuid

import auto_reject_talk
import sqlalchemy
from alembic import config, script
from alembic.runtime import migration
from flask import Flask, request
from flask_migrate import Migrate
from flask_restx import Api, Resource, fields
from UnleashClient import UnleashClient
from werkzeug.middleware.proxy_fix import ProxyFix

from database import talk_request_db
from database.database import db
from database.talk_request_db import TalkRequest
from web_app.config import load_config

app = Flask(__name__)
app_config = load_config()
app.config.from_object(app_config)
app.wsgi_app = ProxyFix(app.wsgi_app, x_for=1, x_host=1)
db.init_app(app)
migrate = Migrate(app, db)  # new
api = Api(app)

unleash_client = UnleashClient(
    app_config.FEATURE_FLAGS_URL,
    app_config.APP_ENVIRONMENT,
    instance_id=app_config.FEATURE_FLAGS_INSTANCE_ID,
    disable_registration=True,
    disable_metrics=True,
)

unleash_client.initialize_client()


class EnumField(fields.String):
    def format(self, value):
        return value.value


address_model = api.model(
    "Address",
    {
        "street": fields.String(description="Street", example="Sunny street 42"),
        "city": fields.String(description="City", example="Sunny city 420000"),
        "state": fields.String(description="State", example="Sunny state"),
        "country": fields.String(description="Country", example="Sunny islands"),
    },
)
talk_request_model = api.model(
    "TalkRequest",
    {
        "id": fields.String(
            description="Unique ID",
            example="354e405c-136f-4e03-b5ce-5f92e3ed3ff8",
            required=True,
        ),
        "event_time": fields.DateTime(
            description="ISO datetime string with timezone",
            required=True,
            example="2021-03-15T06:55:11.777386+00:00",
        ),
        "address": fields.Nested(
            address_model,
            description="Address of the venue",
            required=True,
        ),
        "topic": fields.String(
            description="Topic of the talk",
            example="Python type checking",
            required=True,
        ),
        "status": EnumField(
            description="Request status",
            example="PENDING",
            required=True,
        ),
        "duration_in_minutes": fields.Integer(
            min=1,
            max=90,
            description="Duration of the talk in minutes",
            example=45,
            required=True,
        ),
        "requester": fields.String(
            description="Email of the requester",
            example="john@doe.com",
            required=True,
        ),
    },
)

request_talk_request_body = api.model(
    "RequestTalkRequestBody",
    {
        "event_time": fields.DateTime(
            description="ISO datetime string with timezone",
            required=True,
            example="2021-03-15T06:55:11.777386+00:00",
        ),
        "address": fields.Nested(
            address_model,
            description="Address of the venue",
            required=True,
        ),
        "topic": fields.String(
            description="Topic of the talk",
            example="Python type checking",
            required=True,
        ),
        "status": EnumField(
            description="Request status",
            example="PENDING",
            required=True,
        ),
        "duration_in_minutes": fields.Integer(
            min=1,
            max=90,
            description="Duration of the talk in minutes",
            example=45,
            required=True,
        ),
        "requester": fields.String(
            description="Email of the requester",
            example="john@doe.com",
            required=True,
        ),
    },
)


@app.route("/health-check/")
def health_check():
    engine = sqlalchemy.create_engine(app.config["SQLALCHEMY_DATABASE_URI"])
    alembic_cfg = config.Config()
    alembic_cfg.set_main_option(
        "script_location",
        str(pathlib.Path(__file__).parent.parent.absolute() / "migrations"),
    )
    db_script = script.ScriptDirectory.from_config(alembic_cfg)
    with engine.begin() as conn:
        context = migration.MigrationContext.configure(conn)
        if context.get_current_revision() != db_script.get_current_head():
            return "Upgrade the database.", 400

    return "OK", 200


@api.route("/request-talk/")
class RequestTalk(Resource):
    @api.doc(body=request_talk_request_body, model=[talk_request_model])
    @api.marshal_with(talk_request_model)
    def post(self):
        talk_request = TalkRequest(
            id=str(uuid.uuid4()),
            event_time=request.json["event_time"],
            address=request.json["address"],
            topic=request.json["topic"],
            status="PENDING",
            duration_in_minutes=request.json["duration_in_minutes"],
            requester=request.json["requester"],
        )
        talk_request = auto_reject_talk.process(talk_request)
        return talk_request.dict(), 201


talk_requests_model = api.model(
    "TalkRequests",
    {
        "results": fields.List(
            fields.Nested(talk_request_model),
            description="Talk requests",
            required=True,
        ),
    },
)


@api.route("/talk-requests/")
class TalkRequests(Resource):
    @api.doc(model=[talk_requests_model])
    @api.marshal_with(talk_requests_model)
    def get(self):
        return {
            "results": [
                talk_request.dict() for talk_request in talk_request_db.list_all()
            ]
        }, 200


accept_talk_request_body = api.model(
    "AcceptTalkRequest",
    {
        "id": fields.String(
            description="Unique ID",
            example="354e405c-136f-4e03-b5ce-5f92e3ed3ff8",
            required=True,
        ),
    },
)


@api.route("/talk-request/accept/")
class AcceptTalkRequest(Resource):
    @api.doc(expect=accept_talk_request_body, model=[talk_request_model])
    @api.marshal_with(talk_request_model)
    def post(self):
        talk_request = talk_request_db.get_by_id(request.json["id"])
        talk_request.accept()
        talk_request = talk_request_db.save(db, talk_request)

        return talk_request, 200


reject_talk_request_body = accept_talk_request_body


@api.route("/talk-request/reject/")
class RejectTalkRequest(Resource):
    @api.doc(expect=reject_talk_request_body, model=[talk_request_model])
    @api.marshal_with(talk_request_model)
    def post(self):
        talk_request = talk_request_db.get_by_id(request.json["id"])
        talk_request.reject()
        talk_request = talk_request_db.save(db, talk_request)

        return talk_request, 200


if __name__ == "__main__":  # pragma: no cover
    app.run(debug=True)
