from sqlalchemy import JSON

from database.database import db
from models import TalkRequest


class TalkRequestModel(db.Model):
    id = db.Column(db.String(36), primary_key=True, index=True, nullable=False)
    event_time = db.Column(db.DateTime(), nullable=False)
    address = db.Column(JSON, nullable=False)
    topic = db.Column(db.String(), nullable=False)
    duration_in_minutes = db.Column(db.SmallInteger(), nullable=False)
    requester = db.Column(db.String(120), nullable=False)
    status = db.Column(db.String(20), nullable=False)


def save(database, talk_request):
    talk_request_model = TalkRequestModel(
        id=talk_request.id,
        event_time=talk_request.event_time,
        address=talk_request.address.dict(),
        topic=talk_request.topic,
        duration_in_minutes=talk_request.duration_in_minutes,
        requester=talk_request.requester,
        status=talk_request.status,
    )
    database.session.merge(talk_request_model)
    database.session.commit()

    return talk_request


def list_all():
    records = TalkRequestModel.query.all()

    return [
        TalkRequest(
            id=record.id,
            event_time=record.event_time,
            address=record.address,
            topic=record.topic,
            duration_in_minutes=record.duration_in_minutes,
            requester=record.requester,
            status=record.status,
        )
        for record in records
    ]


def get_by_id(talk_request_id):
    record = TalkRequestModel.query.filter_by(id=talk_request_id).first()

    return TalkRequest(
        id=record.id,
        event_time=record.event_time,
        address=record.address,
        topic=record.topic,
        duration_in_minutes=record.duration_in_minutes,
        requester=record.requester,
        status=record.status,
    )
