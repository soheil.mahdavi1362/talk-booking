output "ecs_security_group_id" {
  value = aws_security_group.ecs.id
}

output "load_balancer_security_group_id" {
  value = aws_security_group.load-balancer.id
}

output "domain_zone_id" {
  value = aws_route53_zone.primary.zone_id
}