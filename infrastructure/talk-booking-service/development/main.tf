terraform {
  backend "http" {}
}

provider "aws" {
  region = var.region
}

data "terraform_remote_state" "vpc" {
  backend = "http"
  config = {
    address = "https://<YOUR GITLAB NAME>/api/v4/projects/13469/terraform/state/vpc"
    username = var.vpc_state_username
    password = var.vpc_state_password
  }
}

module "talk-booking-service" {
  source = "../../modules/talk-booking-service"

  vpc_id = "vpc-0d39de604b327e4e1"
  ecs_security_group_id = data.terraform_remote_state.vpc.outputs.ecs_security_group_id
  load_balancer_security_group_id = data.terraform_remote_state.vpc.outputs.load_balancer_security_group_id
  public_subnet_1_id = "subnet-03547634bd88bd6cd"
  public_subnet_2_id = "subnet-0a35fb458f17b8d19"
  private_subnet_1_id = "subnet-0d6782450ee979f67"
  private_subnet_2_id = "subnet-0476517b48dc8b9cc"
  instance_type = "t3.small"
  log_retention_in_days = 30
  autoscale_min = 1
  autoscale_desired = 1
  autoscale_max = 4
  region = var.region
  app_count = 1
  environment_name = "talk-booking-dev"
  app_environment = "development"
  domain_zone_id = data.terraform_remote_state.vpc.outputs.domain_zone_id
  domain = "develop-1234"
}