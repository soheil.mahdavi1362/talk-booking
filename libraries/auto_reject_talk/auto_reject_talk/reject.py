def process(talk_request):
    if not (20 <= talk_request.duration_in_minutes <= 90):
        talk_request.reject()

    return talk_request
